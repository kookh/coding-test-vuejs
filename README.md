1. 네이버 실시간 검색어 가져오기
2. 유효한 주민등록 번호인지 판단

- 주민등록번호 저장 / 리스트 / 삭제

  ![실시간 검색어](./image/keywords.png)
  ![주민번호 검증](./image/jumin.png)


### 기술스택

##### vue.js, vuetify, vuex, vue-router, axios

##### 1. 라우터

##### src/router

##### 2. 백엔드 API 호출

##### src/store/action.js

##### 3. 컴포넌트

##### src/components/page

###### 실시간 검색어 페이지

###### 주민등록번호 검증 페이지

##### 4. 배포 (AWS S3)
