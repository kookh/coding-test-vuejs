import axios from "axios";

export default process.env.NODE_ENV == "production" ?
  axios.create({
    baseURL: "http://codingtest-env.eba-gvxywjay.ap-northeast-2.elasticbeanstalk.com/api/v1"
    // baseURL: "http://127.0.0.1:3000/api/v1"
  }) :
  axios.create({
    baseURL: "http://127.0.0.1:3000/api/v1"
  });