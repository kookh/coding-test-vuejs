import { SET_KEYWORDS, SET_JUMINS } from "./mutations-types";
import api from "@/api";

export default {
  //실시간 검색어 가져오기
  async getKeywords({ commit }) {
    return await api
      .get("/realtime")
      .then((res) => {
        commit(SET_KEYWORDS, res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  },

  //주민등록 번호 검증
  async verifyJumin({ commit }, payload) {
    const { jumin } = payload;
    return await api
      .post("/jumin", {
        jumin,
      })
      .then(async (res) => {
        if (res.data.data.isRightJumin === true) {
          alert(res.data.data.juminNumber + "는 올바른 주민 번호 입니다.");
        } else {
          alert(res.data.data.juminNumber + "는 잘못된 주민 번호 입니다.");
        }
        return await api
          .get("/jumin")
          .then((res) => {
            commit(SET_JUMINS, res.data);
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  },

  //모든 주민번호 가져오기
  async getJuminList({ commit }) {
    return await api
      .get("/jumin")
      .then((res) => {
        commit(SET_JUMINS, res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  },

  //주민등록 번호 삭제
  async deleteJumin({ commit }, id) {
    return await api
      .delete(`/jumin/${id}`)
      .then(async () => {
        return await api
          .get("/jumin")
          .then((res) => {
            commit(SET_JUMINS, res.data);
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  },
};
