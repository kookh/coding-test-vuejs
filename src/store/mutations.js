import {
    SET_KEYWORDS,
    SET_JUMINS,
} from './mutations-types'

export default {
    [SET_KEYWORDS](state, keywords) {
        if (keywords) {
            state.keywords = keywords
        }
    },
    [SET_JUMINS](state, jumins) {
        if (jumins) {
            state.jumins = jumins
        }
    },
}