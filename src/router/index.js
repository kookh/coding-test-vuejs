import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/page/MainPage'
import JuminPage from '@/components/page/JuminPage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
      path: '/',
      component: MainPage
    },
    {
      path: '/jumin',
      component: JuminPage
    },
  ]
})